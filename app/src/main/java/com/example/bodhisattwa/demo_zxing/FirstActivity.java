package com.example.bodhisattwa.demo_zxing;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.zxing.client.android.CaptureActivity;

public class FirstActivity extends Activity {
    public static final String EXTRA_MESSAGE_From_First_Scan = "com.example.myfirstapp.MESSAGE";
    public final static String SCAN_TYPE = "com.example.scan.type";
    private final int _splashTime = 3000;

    private TextView textView,textView2;
    private Button /**buttonNewOrder,*/buttonSettings;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_first);
        Intent intentFromPrev = getIntent();
        String scanType = intentFromPrev.getStringExtra(MainActivity.SCAN_TYPE);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        textView = (TextView)findViewById(R.id.textView1);
        textView2 = (TextView)findViewById(R.id.textView2);
        //buttonNewOrder = (Button)findViewById(R.id.button1);
        buttonSettings = (Button)findViewById(R.id.button2);

        if(new String(scanType).equals("camera")){
            Intent intent = new Intent(getApplicationContext(),CaptureActivity.class);
            intent.setAction("com.google.zxing.client.android.SCAN");
            intent.putExtra("SAVE_HISTORY", false);
            startActivityForResult(intent, 0);
        }else if(new String(scanType).equals("keyboard")){
            _showOptionDialogTextInput();
        }else{
            Log.d("Personal","do nothing");
        }
        /**
        buttonNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _showOptionDialogButton();
            }
        });*/
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                final String contents = data.getStringExtra("SCAN_RESULT");
                //textView.setText("Scan data received : "+contents);
                textView.setText(contents);
                textView2.setText("Proceeding for second scan...");
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //finish();
                        _showOptionDialogButton();
                        //Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                        //intent.putExtra(EXTRA_MESSAGE_From_First_Scan, contents);
                        //startActivity(intent);
                    }
                }, _splashTime);


            } else if (resultCode == RESULT_CANCELED) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
    public void _showOptionDialogTextInput() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_text_view, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        //final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        final EditText edtText = (EditText) dialogView.findViewById(R.id.editText);
        final Button btnSave = (Button)dialogView.findViewById(R.id.buttonSave);

        dialogBuilder.setTitle("Choose an option");

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                //dialog.dismiss();
            }
        });
        final AlertDialog b = dialogBuilder.create();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(edtText.getText().toString());
                textView2.setText("Proceeding for second scan...");
                Log.d("message",edtText.getText().toString());
                b.dismiss();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //finish();
                        _showOptionDialogButton();
                        //Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                        //intent.putExtra(EXTRA_MESSAGE_From_First_Scan, contents);
                        //startActivity(intent);
                    }
                }, _splashTime);

            }
        });
        b.show();
    }
    public void _showOptionDialogButton() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_button, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        //final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        final Button btnKeyboard = (Button)dialogView.findViewById(R.id.buttonKeyboard);
        final Button btnCamera = (Button)dialogView.findViewById(R.id.buttonCamera);

        dialogBuilder.setTitle("Choose an option");

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                //dialog.dismiss();
            }
        });
        btnKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("working","bodhi");
                //SharedPreferences.Editor editor = sharedPreferences.edit();
                //editor.putString(getString(R.string.scan_type), "keyboard");
                //editor.commit();
                Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                intent.putExtra(EXTRA_MESSAGE_From_First_Scan, textView.getText().toString());
                intent.putExtra(SCAN_TYPE,"keyboard");
                startActivity(intent);
            }
        });
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SharedPreferences.Editor editor = sharedPreferences.edit();
                //editor.putString(getString(R.string.scan_type), "camera");
                //editor.commit();
                Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                intent.putExtra(EXTRA_MESSAGE_From_First_Scan, textView.getText().toString());
                intent.putExtra(SCAN_TYPE,"camera");
                startActivity(intent);
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
