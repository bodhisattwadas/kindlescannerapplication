package com.example.bodhisattwa.demo_zxing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.google.zxing.client.android.CaptureActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import static android.content.ContentValues.TAG;

public class SecondActivity extends Activity {
    private String  messageFromFirstActivity = "";
    private String messageFromSecondActivity = "";
    private Button buttonSettings,buttonHome;
    private ProgressDialog pd;
    private Context context;
    private TextView textView1;
    private SharedPreferences sharedPreferences;
    private final List<NameValuePair> params = new ArrayList<NameValuePair>();
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_second);
        context = this;
        Intent intent = getIntent();
        messageFromFirstActivity = intent.getStringExtra(FirstActivity.EXTRA_MESSAGE_From_First_Scan);
        String scanType = intent.getStringExtra(FirstActivity.SCAN_TYPE);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        buttonHome = (Button)findViewById(R.id.button1);
        buttonSettings = (Button)findViewById(R.id.button2);

        textView1 = (TextView)findViewById(R.id.textView);

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });


        if(new String(scanType).equals("camera")){
            Intent intent2 = new Intent(getApplicationContext(),CaptureActivity.class);
            intent2.setAction("com.google.zxing.client.android.SCAN");
            intent2.putExtra("SAVE_HISTORY", false);
            startActivityForResult(intent2, 0);
        }else if(new String(scanType).equals("keyboard")){
            _showOptionDialogTextInput();
        }else{
            Log.d("Personal","do nothing");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                messageFromSecondActivity = data.getStringExtra("SCAN_RESULT");
                Log.d(TAG, "firstContents: " + messageFromFirstActivity);
                Log.d(TAG, "secondContents: " + messageFromSecondActivity);
                Log.d(TAG, "POST URL :"+sharedPreferences.getString(getString(R.string.post_url),"http://www.ljdtracking.com/scanner/"));
                new POSTBarcode().execute(messageFromFirstActivity,messageFromSecondActivity);

            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "RESULT_CANCELED");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    private class POSTBarcode extends AsyncTask<String, String, String> {

        private final List<NameValuePair> parameter = new ArrayList<NameValuePair>();
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage("Please wait..");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            parameter.add(new BasicNameValuePair("scan1", params[0]));
            parameter.add(new BasicNameValuePair("scan2", params[1]));
            JSONParser jsonParser = new JSONParser();
            jsonObject = jsonParser.makeHttpRequest(sharedPreferences.getString(getString(R.string.post_url),"http://www.ljdtracking.com/scanner/"),"GET",parameter);
            return null;
        }

        protected void onPostExecute(String result) {
            if (pd!=null) {
                pd.dismiss();
            }
            try {
                String getMessage = jsonObject.getString("Result");
                String getScan1 = jsonObject.getString("scan1");
                Log.d("Server Response ",getMessage);
                Log.d("Server Response ",getScan1);
                textView1.setText(getMessage);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }

    }
    public void _showOptionDialogTextInput() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_text_view, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        //final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        final EditText edtText = (EditText) dialogView.findViewById(R.id.editText);
        final Button btnSave = (Button)dialogView.findViewById(R.id.buttonSave);

        dialogBuilder.setTitle("Choose an option");

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                //dialog.dismiss();
            }
        });
        final AlertDialog b = dialogBuilder.create();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("message",edtText.getText().toString());
                new POSTBarcode().execute(messageFromFirstActivity,edtText.getText().toString());
                b.dismiss();
            }
        });
        b.show();
    }

 }
